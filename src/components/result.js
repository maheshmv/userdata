import React from "react";
import "../result.css";
import {Divider, Header, Icon } from 'semantic-ui-react'
const Result = ({user}) =>{
    return (
      <div className="UserCard">
	  
        <div className="UserCard-id">Id - {user.id}</div>
		<div className="UserCard-username">{user.username}</div>
		
        <h1 className="UserCard-name">
          {user.name}
        </h1>
		<div className="UserCard-email"><Icon name='phone' size='large'/> <span>{user.phone}</span></div>
		<div className="UserCard-email"><Icon name='desktop' size='large'/> <span>{user.website}</span></div>
		<div className="UserCard-email"><Icon name='at' size='large'/> <span>{user.email}</span></div>
		<div className="UserCard-email"><Icon name='briefcase' size='large'/> <span>{user.company.name}</span></div>
		<Divider horizontal>
		  <Header as='h4'>
			<Icon name='address card' size='large'/>
			Address
		  </Header>
		</Divider>
		
		<div className="UserCard-address">
			{user.address.suite}, {user.address.street} Street, {user.address.city}. 
				({user.address.zipcode})
		</div>
      </div>
    );
}

export default Result;
