import React from 'react';
import './App.css';
import SearchBar from "./components/search";
import Result from "./components/result";

class ContactSearch extends React.Component {
  constructor() {
    super();
    this.state = {
	  userData : [],
	  userCompanyName : {},
      searchDone: false,
      errorMessage: ""
    };

    this.callUserData = this.callUserData.bind(this);
  }
	
  callUserData(user) {
    const url = `https://jsonplaceholder.typicode.com/users?username=${user}`;
	

    fetch(url)
      .then(handleErrors)
      .then(resp => resp.json())
      .then(data => {
		if(data.length ==0)
		{
			throw Error(`The given Username is not found in records. Username is case sensitive`);
		}
        this.setState({
          userData: [...data],
          searchDone: true,
          errorMessage: ""
        });
      })
      .catch(error => {
        // If an error is catch, it's sent to SearchBar as props
        this.setState({ errorMessage: error.message });
      });

    function handleErrors(response) {
      if (!response.ok) {
		  console.log(response.statusText);
        throw Error(response.statusText);
      }
      return response;
    }
  }
  
  render() {
    const { searchDone, userData, errorMessage } = this.state;
		return (
		  <div className="ContactSearch">
			<SearchBar
			  callBackFromParent={this.callUserData}
			  error={errorMessage}
			/>
			{
				searchDone && userData && userData.map((u)=>{ 
					return(
						<Result user={u} />
					);
				})
			}
		  </div>
		);
	}
}

export default ContactSearch;
