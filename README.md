This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Download code

You can download from bitbucket repo using command 
git clone git@bitbucket.org:maheshmv/userdata.git

### `git clone git@bitbucket.org:maheshmv/userdata.git`

Goto project directory in command prompt and enter
### `npm install`

After the installation is over enter 
### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3006] to check the application it in the browser.

Enter the username from the https://jsonplaceholder.typicode.com/users. 
Application is configured to return the data available for the users on the given page. You need to search by the username.
